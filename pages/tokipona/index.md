% toki pona
% /dev/urandom
% march 2020

This is the a series of pages comprising an attempt at an educational course
about *toki pona*, a constructed language originally designed in 2001 and
then gradually revised over the years by Sonja Lang.

The language is designed around the ideas of minimalist design and simplifying
one's thoughts by breaking down complicated ideas into their basic components.
It only uses 120 "official words" (with a few additional ones being
sometimes used by the community), has an incredibly simple grammar and uses few
sounds that are hard to confuse.

As a result, the language is considered to be incredibly easy to learn, with
some people claiming to be able to read it after only days and achieving fluency
within a week or two.

However, with that simplicity also come limitations. Many words have multiple
meanings, and some phrases or sentences are ambiguous without context.
Expressing many concepts and ideas in toki pona will require one to come up with
their own phrases or rephrase them completely (which, as mentioned before, is
part of the language's idea).

Speaking of context, toki pona is a very context-sensitive language. Different
people may describe the same basic ideas or things in completely different ways.
This is also part of the language's idea. Even some of the rules of the language
are also interpreted differently by different people, whether depending on what
their native language is or their opinions on what's the best way to communicate
something.

In addition, toki pona is also designed to be easy to use regardless of one's
native language. The sounds and syllable structure used in toki pona are
distinct from one another and common across many languages, whereas the
vocabulary features words borrowed from many languages across the world.

# About this course

There are several good sources to educate yourself about toki pona available
already. The most important (and best, in my opinion), is the [official toki
pona book](https://tokipona.org/) (also known as "pu") published in 2014 by
Sonja Lang herself. It is not free, but it's a well-written book with lots of
additional texts to read and it explains the language very well.

Another useful resource is the online course ["o kama sona e toki
pona!"](http://tokipona.net/tp/janpije/okamasona.php) (learn toki pona!) by
Bryant Knight (aka "jan Pije"). It has some differences in how it uses certain
words, and the past versions of the course have attracted some controversy over
their bigoted content, but it's also a well-made course.

My goal here is to try and present a version that tries to account for the
different ways people speak and write toki pona and the way it is being used
now. Some pages will include "Dialectal differences" sections, in which these
differences will be covered. Some of the larger differences will be described
right away. I will provide my personal opinions on some of these differences, so
while this course does try to be exhaustive, it is not impartial.

The page numbered zero will provide basic info on the language's spelling and
pronunciation, and each page past that will introduce 10 words from the
language's 120 word dictionary.

## Table of Contents

### Course pages

* [page 0 - spelling and pronunciation](0.html) [(普通话)](zh_0.html)

* [page 1 - basic sentences](1.html)

* [page 2 - adjectives](2.html)

* [page 3 - verbs and objects](3.html)

* [page 4 - oh no! more vocabulary](4.html)

* [page 5 - this and that](5.html)

* [page 6 - prepositions and locations](6.html)

* [page 7 - interjections, questions, commands and names](7.html)

* * [page 7a - more about making unofficial words](7a.html)

* [page 8 - colorful language](8.html)

* [page 9 - complex adjectives and contexts](9.html)

* [page 10 - pre-verbs and time](10.html)

* [page 11 - numbers](11.html)

* [page 12 - the final countdown](12.html)

### Extra pages

* [extra page 1 - old and new words](x1.html)

* [extra page 2 - other writing systems](x2.html)

* [credits and acknowledgements](credits.html)

## Useful resources

Apart from the above-mentioned book and courses, here are some good resources
and links for people who want to learn or use toki pona:

* [tokipona.net](http://tokipona.net) (warning: if your "HTTPS Everywhere" addon
  is set to "Encrypt All Sites Eligible", disable it for tokipona.net, or else
  it will redirect you to an unrelated website)

* [/r/tokipona subreddit](https://reddit.com/r/tokipona)

* ["ma pona pi toki pona" Discord server](https://discord.gg/XKzj3ex)

* [toki pona Telegram group](https://telegram.me/joinchat/BLVsYz92zHUp2h2TYp9kTA)

