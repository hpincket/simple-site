% toki pona extra page 2 - other writing systems
% /dev/urandom
% april 2020

<style>
@font-face {
    font-family: "sitelen pona";
    src: url("linjapimeja19.ttf")
}

.sp {
    font-size:3em;
    font-family:"sitelen pona";
    font-variant-ligatures: common-ligatures;
}

.spflex {
display: none;
flex-direction: row;
flex-wrap: wrap;
border: 2px solid #9b9b9b;
border-radius: 4pt;
}

.spitem {
	flex-grow: 1;
	width: 4em;
	padding: 0.25em;
	margin: 0.5em;
	border: 1px solid #9b9b9b;
	border-radius: 4pt;
	text-align: center;
	overflow: hidden;
}

@media tty {
	.spflex {
		display: none !important;
	}
}

</style>

While the most common writing system for toki pona by far is the Latin alphabet,
there have been others adapted, or created specifically, for toki pona.

## Adapted writing systems

With toki pona only using 5 vowel and 9 consonant sounds, converting another
language's writing system to toki pona can become rather easy. For example,
here's how they can be converted to Greek and Cyrillic:

| Latin | Greek | Cyrillic |
|:-----:|:-----:|:--------:|
| a | α | а |
| e | ε | е |
| i | ι | и |
| j | γ | й |
| k | κ | к |
| l | λ | л |
| m | μ | м |
| n | ν | н |
| o | ο | о |
| p | π | п |
| s | σ | с |
| t | τ | т |
| u | υ | у |
| w | β | в |

The language also only has 92 possible syllables (47 if "-n" is treated as a
separate syllable). This means it can also be adapted for many syllabic writing
systems as well.

For example, here are some suggestions for a way to write toki pona using
Hangul, the writing system of Korean. (While in features symbols representing
individual sounds much like an alphabet, they're arranged in syllabic blocks.)

* ["Writing Toki Pona with Korean Hangul" (archived)][hangularch]
* ["Hangul for Toki Pona" on Reddit][hangulred]

[hangularch]:https://web.archive.org/web/20070313181500/http://www.tokipona.bravehost.com/korean.html
[hangulred]:https://www.reddit.com/r/tokipona/comments/8mx951/hangul_for_toki_pona/

Or here is a page on converting toki pona to Devanagari:

* ["Toki Pona in Devanāgarī" (archived)][devanagari]

[devanagari]:https://web.archive.org/web/20060727115116/http://www.deadlybrain.org/projects/tokipona/deva_guja.php

With some relatively small changes in sounds, it can also be written with the
Japanese Hiragana system, as proposed here:

* ["Hiragana for Toki Pona"][hiragana1]
* ["sitelen Hiragana (ひらがな)" on Reddit][hiragana_red]

[hiragana1]:https://www.deviantart.com/derroflcopter/journal/Hiragana-for-Toki-Pona-339541633
[hiragana_red]:https://www.reddit.com/r/tokipona/comments/e7g91u/sitelen_hiragana_%E3%81%B2%E3%82%89%E3%81%8C%E3%81%AA/

## sitelen pona

The most common writing system created for toki pona is the logographic *sitelen pona*
("simple writing"), created by Sonja Lang herself and published in the official
book.

> %info%
> The part of the book describing sitelen pona was published with a
> non-commercial [CC-BY-NC 4.0](https://creativecommons.org/licenses/by-nc/4.0/) 
> license. Hence, it's easily available online in other courses,
> such as ["o kama sona e toki pona!"'s
> page](http://tokipona.net/tp/janpije/hieroglyphs.php) on the system, which
> describes it almost exactly the same as the official book.
>

<noscript>
<div class="error">
Your web browser has JavaScript disabled. Without it, this page cannot check
whether or not the "linja pimeja" font has successfully loaded. Therefore, the
sitelen pona table will remain hidden.
</div>
</noscript>

<div class="spflex">
<div class="spitem"><div class="sp">a</div>		a/kin</div>
<div class="spitem"><div class="sp">akesi</div>		akesi</div>
<div class="spitem"><div class="sp">ala</div>		ala</div>
<div class="spitem"><div class="sp">alasa</div>		alasa</div>
<div class="spitem"><div class="sp">ale</div>		ale/ali</div>
<div class="spitem"><div class="sp">anpa</div>		anpa</div>
<div class="spitem"><div class="sp">ante</div>		ante</div>
<div class="spitem"><div class="sp">anu</div>		anu</div>
<div class="spitem"><div class="sp">awen</div>		awen</div>
<div class="spitem"><div class="sp">e</div>		e</div>
<div class="spitem"><div class="sp">en</div>		en</div>
<div class="spitem"><div class="sp">esun</div>		esun</div>
<div class="spitem"><div class="sp">ijo</div>		ijo</div>
<div class="spitem"><div class="sp">ike</div>		ike</div>
<div class="spitem"><div class="sp">ilo</div>		ilo</div>
<div class="spitem"><div class="sp">insa</div>		insa</div>
<div class="spitem"><div class="sp">jaki</div>		jaki</div>
<div class="spitem"><div class="sp">jan</div>		jan</div>
<div class="spitem"><div class="sp">jelo</div>		jelo</div>
<div class="spitem"><div class="sp">jo</div>		jo</div>
<div class="spitem"><div class="sp">kala</div>		kala</div>
<div class="spitem"><div class="sp">kalama</div>	kalama</div>
<div class="spitem"><div class="sp">kama</div>		kama</div>
<div class="spitem"><div class="sp">kasi</div>		kasi</div>
<div class="spitem"><div class="sp">ken</div>		ken</div>
<div class="spitem"><div class="sp">kepeken</div>	kepeken</div>
<div class="spitem"><div class="sp">kili</div>		kili</div>
<div class="spitem"><div class="sp">kiwen</div>		kiwen</div>
<div class="spitem"><div class="sp">ko</div>		ko</div>
<div class="spitem"><div class="sp">kon</div>		kon</div>
<div class="spitem"><div class="sp">kule</div>		kule</div>
<div class="spitem"><div class="sp">kulupu</div>	kulupu</div>
<div class="spitem"><div class="sp">kute</div>		kute</div>
<div class="spitem"><div class="sp">la</div>		la</div>
<div class="spitem"><div class="sp">lape</div>		lape</div>
<div class="spitem"><div class="sp">laso</div>		laso</div>
<div class="spitem"><div class="sp">lawa</div>		lawa</div>
<div class="spitem"><div class="sp">len</div>		len</div>
<div class="spitem"><div class="sp">lete</div>		lete</div>
<div class="spitem"><div class="sp">li</div>		li</div>
<div class="spitem"><div class="sp">lili</div>		lili</div>
<div class="spitem"><div class="sp">linja</div>		linja</div>
<div class="spitem"><div class="sp">lipu</div>		lipu</div>
<div class="spitem"><div class="sp">loje</div>		loje</div>
<div class="spitem"><div class="sp">lon</div>		lon</div>
<div class="spitem"><div class="sp">luka</div>		luka</div>
<div class="spitem"><div class="sp">lukin</div>		lukin</div>
<div class="spitem"><div class="sp">lupa</div>		lupa</div>
<div class="spitem"><div class="sp">ma</div>		ma</div>
<div class="spitem"><div class="sp">mama</div>		mama</div>
<div class="spitem"><div class="sp">mani</div>		mani</div>
<div class="spitem"><div class="sp">meli</div>		meli</div>
<div class="spitem"><div class="sp">mi</div>		mi</div>
<div class="spitem"><div class="sp">mije</div>		mije</div>
<div class="spitem"><div class="sp">moku</div>		moku</div>
<div class="spitem"><div class="sp">moli</div>		moli</div>
<div class="spitem"><div class="sp">monsi</div>		monsi</div>
<div class="spitem"><div class="sp">mu</div>		mu</div>
<div class="spitem"><div class="sp">mun</div>		mun</div>
<div class="spitem"><div class="sp">musi</div>		musi</div>
<div class="spitem"><div class="sp">mute</div>		mute</div>
<div class="spitem"><div class="sp">nanpa</div>		nanpa</div>
<div class="spitem"><div class="sp">nasa</div>		nasa</div>
<div class="spitem"><div class="sp">nasin</div>		nasin</div>
<div class="spitem"><div class="sp">nena</div>		nena</div>
<div class="spitem"><div class="sp">ni</div>		ni</div>
<div class="spitem"><div class="sp">nimi</div>		nimi</div>
<div class="spitem"><div class="sp">noka</div>		noka</div>
<div class="spitem"><div class="sp">o</div>		o</div>
<div class="spitem"><div class="sp">olin</div>		olin</div>
<div class="spitem"><div class="sp">ona</div>		ona</div>
<div class="spitem"><div class="sp">open</div>		open</div>
<div class="spitem"><div class="sp">pakala</div>	pakala</div>
<div class="spitem"><div class="sp">pali</div>		pali</div>
<div class="spitem"><div class="sp">palisa</div>	palisa</div>
<div class="spitem"><div class="sp">pan</div>		pan</div>
<div class="spitem"><div class="sp">pana</div>		pana</div>
<div class="spitem"><div class="sp">pi</div>		pi</div>
<div class="spitem"><div class="sp">pilin</div>		pilin</div>
<div class="spitem"><div class="sp">pimeja</div>	pimeja</div>
<div class="spitem"><div class="sp">pini</div>		pini</div>
<div class="spitem"><div class="sp">pipi</div>		pipi</div>
<div class="spitem"><div class="sp">poka</div>		poka</div>
<div class="spitem"><div class="sp">poki</div>		poki</div>
<div class="spitem"><div class="sp">pona</div>		pona</div>
<div class="spitem"><div class="sp">pu</div>		pu</div>
<div class="spitem"><div class="sp">sama</div>		sama</div>
<div class="spitem"><div class="sp">seli</div>		seli</div>
<div class="spitem"><div class="sp">selo</div>		selo</div>
<div class="spitem"><div class="sp">seme</div>		seme</div>
<div class="spitem"><div class="sp">sewi</div>		sewi</div>
<div class="spitem"><div class="sp">sijelo</div>	sijelo</div>
<div class="spitem"><div class="sp">sike</div>		sike</div>
<div class="spitem"><div class="sp">sin</div>		sin</div>
<div class="spitem"><div class="sp">sina</div>		sina</div>
<div class="spitem"><div class="sp">sinpin</div>	sinpin</div>
<div class="spitem"><div class="sp">sitelen</div>	sitelen</div>
<div class="spitem"><div class="sp">sona</div>		sona</div>
<div class="spitem"><div class="sp">soweli</div>	soweli</div>
<div class="spitem"><div class="sp">suli</div>		suli</div>
<div class="spitem"><div class="sp">suno</div>		suno</div>
<div class="spitem"><div class="sp">supa</div>		supa</div>
<div class="spitem"><div class="sp">suwi</div>		suwi</div>
<div class="spitem"><div class="sp">tan</div>		tan</div>
<div class="spitem"><div class="sp">taso</div>		taso</div>
<div class="spitem"><div class="sp">tawa</div>		tawa</div>
<div class="spitem"><div class="sp">telo</div>		telo</div>
<div class="spitem"><div class="sp">tenpo</div>		tenpo</div>
<div class="spitem"><div class="sp">toki</div>		toki</div>
<div class="spitem"><div class="sp">tomo</div>		tomo</div>
<div class="spitem"><div class="sp">tu</div>		tu</div>
<div class="spitem"><div class="sp">unpa</div>		unpa</div>
<div class="spitem"><div class="sp">uta</div>		uta</div>
<div class="spitem"><div class="sp">utala</div>		utala</div>
<div class="spitem"><div class="sp">walo</div>		walo</div>
<div class="spitem"><div class="sp">wan</div>		wan</div>
<div class="spitem"><div class="sp">waso</div>		waso</div>
<div class="spitem"><div class="sp">wawa</div>		wawa</div>
<div class="spitem"><div class="sp">weka</div>		weka</div>
<div class="spitem"><div class="sp">wile</div>		wile</div>
</div>

<script>
document.fonts.load("12pt 'sitelen pona'").then(function () {
  var box = document.getElementsByClassName('spflex');
  for (var i=0; i < box.length; i++)
   box[i].style.display = "flex"; 
});
</script>

Much like the Latin alphabet, it is written left-to-right and top-to-bottom.
Each character represents one word (or sometimes even a phrase), or one letter
in a proper name.

Since toki pona's basic dictionary only uses 120 words, there are only 120
characters one needs to learn. And most of these characters are, in one way or
another, direct representations of the words they mean.

For example, "lawa", meaning "head", is literally a symbol of a head with a cap
on. "nanpa", meaning "number", is based on the "#" number sign, etc.

An adjective character can be put inside or over/under a noun character to
represent a noun phrase.

> %info%
> You might notice that toki pona's "logo", used on the cover of the official
> book and on most websites to represent it, is, in fact, sitelen pona's
> composite character for "toki pona", with the "pona" symbol written inside the
> "toki" symbol.

Unofficial words are written inside a "cartouche" symbol (a rounded shape that
surrounds all the characters), with characters for words that start with their
first letters. For the example linked above (and used in the official book), "ma
Kanata" is written as "ma [kasi alasa nasin awen telo a]".

Since the question mark is used as the character for "seme", question sentences
may be ended with a period (or a smaller question mark) instead, depending on
the text.

Here's some basic text written in sitelen pona. 

> %warning%
> (If your browser is unable to load the ["linja pimeja" font](https://github.com/increpare/linja_pimeja), 
> the text below would just show up in large Latin characters.

> %sp%
> wan ni pi lipu ni li sitelen kepeken sitelen pona. sina ken ala ken sona e ni.
>

<a name="answers" href="#answers" onclick="revealSpoilers();">Reveal translation</a>

> %spoiler%
> This part of this document is written using sitelen pona. Can you understand
> it?

For some other texts written in sitelen pona, including a page that tries to
teach someone to read it without using any other writing system, check out the
website ["tomo pi sitelen pona"](https://davidar.github.io/tp/) by jan Tepu.

## sitelen sitelen

Jonathan Gabel's ["sitelen sitelen"](https://jonathangabel.com/toki-pona/)
writing system was designed as a more aesthetically pleasant method to write texts in
toki pona. It's a non-linear system visually inspired by the Mayan script.

Compared to writing toki pona in Latin alphabet or sitelen pona, sitelen sitelen
is significantly more difficult to understand, and therefore is only used rarely
by the community. However, the impressive visual style of texts written in it --
such as [this
contract](https://www.jonathangabel.com/archive/2012/artworks_lipu-lawa-pi-esun-kama.html)
or the toki pona
[proverbs](https://jonathangabel.com/toki-pona/dictionaries/gallery/) -- many of
which are also used in the official book -- cannot be denied.

[Top page](index.html)

